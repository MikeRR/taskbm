package com.letgetitstarted.app.entity;


import java.util.Objects;

public class Url {

    private String url;
    private String data;

    public Url() {
    }

    public Url(String url, String data) {
        this.url = url;
        this.data = data;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Url url1 = (Url) o;
        return Objects.equals(url, url1.url) &&
                Objects.equals(data, url1.data);
    }

    @Override
    public int hashCode() {

        return Objects.hash(url, data);
    }
}
