package com.letgetitstarted.app.repository;

import com.mchange.v2.c3p0.ComboPooledDataSource;

import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

public class ConnectionManager {

    private ComboPooledDataSource dataSource;

    private static ConnectionManager instance = null;

    public static ConnectionManager getInstance() {
        if(instance == null) {
            instance = new ConnectionManager();
            instance.init();
        }
        return instance;
    }

    private void init(){
        Properties props = new Properties();
        try(FileInputStream fis
                    = new FileInputStream(getClass().getResource("/db.properties").getPath())) {
            props.load(fis);
            dataSource = new ComboPooledDataSource();
            dataSource.setUser(props.getProperty("MYSQL_DB_USERNAME"));
            dataSource.setPassword(props.getProperty("MYSQL_DB_PASSWORD"));
            dataSource.setDriverClass("com.mysql.jdbc.Driver");
            dataSource.setJdbcUrl(props.getProperty("MYSQL_DB_URL"));
            dataSource.setMinPoolSize(Integer.parseInt(props.getProperty("MYSQL_DB_MIN_POOL_SIZE")));
            dataSource.setAcquireIncrement(2);
            dataSource.setMaxPoolSize(Integer.parseInt(props.getProperty("MYSQL_DB_MAX_POOL_SIZE")));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Connection getConnection() throws SQLException {
        return dataSource.getConnection();
    }
}
