package com.letgetitstarted.app.repository;

import com.letgetitstarted.app.entity.Url;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UrlDAO {

    private static UrlDAO INSTANCE;

    public static UrlDAO getInstance(){
        if(INSTANCE == null) {
            INSTANCE = new UrlDAO();
        }
        return INSTANCE;
    }

    public void saveUrl(Url url) throws Exception {
        try(Connection connection = ConnectionManager.getInstance().getConnection();
            PreparedStatement stmt = connection.prepareStatement("insert into url (url, data) values (?, ?)")){
            stmt.setString(1, url.getUrl());
            stmt.setString(2, url.getData());
            stmt.executeUpdate();
        } catch (SQLException e) {
            throw new Exception(e);
        }
    }

    public List<Url> getUrl(Url url) throws Exception {
        List<Url> result = new ArrayList<>();
        String query;

        if (url == null) {
            query = "Select * from url";
        } else {
            query = "Select * from url where url = (?)";
        }

        try(Connection connection = ConnectionManager.getInstance().getConnection();
            PreparedStatement statement = createPreparedStatement(url, connection, query);
            ResultSet resultSet = statement.executeQuery()) {

            while(resultSet.next()) {
                result.add(new Url(resultSet.getString("url"), resultSet.getString("data")));
            }

        } catch (SQLException e) {
            throw new Exception(e);
        }
        return result;
    }

    private PreparedStatement createPreparedStatement(Url url, Connection connection, String query) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(query);

        if (url != null) {
            statement.setString(1, url.getUrl());
        }

        return statement;
    }




}
