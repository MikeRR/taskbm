package com.letgetitstarted.app.utils;

import org.apache.commons.validator.routines.UrlValidator;

public class ValidatorUtils {

    private static final UrlValidator URL_VALIDATOR = new UrlValidator();

    public static boolean isValidURL(String url){
        return URL_VALIDATOR.isValid(url);
    }
}
