package com.letgetitstarted.app.utils;

import com.google.gson.Gson;
import com.letgetitstarted.app.entity.Url;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.util.List;

public class ParsingDataUtils {

    public static Url parseDataToUrlObject(HttpServletRequest request) {
        StringBuffer jb = new StringBuffer();
        String line;
        try(BufferedReader reader = request.getReader()) {
            while ((line = reader.readLine()) != null)
                jb.append(line);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        String data = String.valueOf(jb);

        if (data.isEmpty()){
            throw new RuntimeException("Body in the request is empty");
        }

        Gson g = new Gson();

        return g.fromJson(data, Url.class);
    }

    public static Url parseDataToUrlObjectGetRequest(HttpServletRequest request) {
        StringBuffer jb = new StringBuffer();
        String line;
        try(BufferedReader reader = request.getReader()) {
            while ((line = reader.readLine()) != null)
                jb.append(line);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        String data = String.valueOf(jb);

        if (!data.isEmpty()){
            Gson g = new Gson();
            return g.fromJson(data, Url.class);
        }

        return null;
    }

    public static String failureResponse(Exception e){
        return "{\"status\": false, \"data\": \""+ e.getMessage() +"\"}";
    }

    public static String successResponse(){
        return "{\"status\": true}";
    }

    public static String successResponseWithUrlData(List<Url> urls){
        if (urls.isEmpty()){
            return "{\"status\": true, \"data\": []}";
        }
        Gson g = new Gson();
        return ("{\"status\": true, \"data\": ") + g.toJson(urls) + "}";
    }

}
