package com.letgetitstarted.app.controller;

import com.letgetitstarted.app.entity.Url;
import com.letgetitstarted.app.repository.UrlDAO;
import com.letgetitstarted.app.utils.ParsingDataUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

import static com.letgetitstarted.app.utils.ParsingDataUtils.parseDataToUrlObjectGetRequest;

public class GetUrlServlet extends HttpServlet {

    public void init() throws ServletException {
        // Do required initialization
    }

    public void destroy() {
        // do nothing.
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Url url = parseDataToUrlObjectGetRequest(req);
        List<Url> result;
        try {
            result = UrlDAO.getInstance().getUrl(url);
        } catch (Exception e) {
            e.printStackTrace();
            resp.getOutputStream().println(ParsingDataUtils.failureResponse(e));
            return;
        }
        resp.getOutputStream().println(ParsingDataUtils.successResponseWithUrlData(result));
    }
}
