package com.letgetitstarted.app.controller;

import com.letgetitstarted.app.entity.Url;
import com.letgetitstarted.app.repository.UrlDAO;
import com.letgetitstarted.app.service.HtmlPageDownloadService;
import com.letgetitstarted.app.utils.ParsingDataUtils;
import com.letgetitstarted.app.utils.ValidatorUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.letgetitstarted.app.utils.ParsingDataUtils.parseDataToUrlObject;

public class SaveUrlServlet extends HttpServlet {

    public void init() throws ServletException {
        // Do required initialization
    }

    public void destroy() {
        // do nothing.
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Url url = parseDataToUrlObject(req);

        if (!ValidatorUtils.isValidURL(url.getUrl())){
            resp.getOutputStream().println(ParsingDataUtils
                    .failureResponse(new RuntimeException("Url in the body of request is not valid")));
            return;
        }

        try {
            url.setData(HtmlPageDownloadService.getPage(url.getUrl()));
            UrlDAO.getInstance().saveUrl(url);
        } catch (Exception e) {
            e.printStackTrace();
            resp.getOutputStream().println(ParsingDataUtils
                    .failureResponse(e));
            return;
        }

        resp.getOutputStream().println(ParsingDataUtils.successResponse());
    }
}
