package com.letgetitstarted.app.service;

import java.io.InputStream;
import java.net.URI;
import java.net.URL;

public class HtmlPageDownloadService {

    public static String getPage(String address) throws Exception{
        URI uri = new URI(address);
        URL url = uri.toURL();

        try(InputStream in = url.openStream()) {

            StringBuilder builder = new StringBuilder();
            byte[] data = new byte[1024];

            int c;
            while ((c = in.read(data, 0, 1024)) != -1) {
                builder.append(new String(data, 0, c));
            }

            return builder.toString();
        }
    }
}
