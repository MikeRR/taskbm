###API V1
#####Conﬁguration 
In the folder ServletApi/src/main/resources/ if ﬁle db.properties. 

Here is next ﬁelds:
MYSQL_DB_USERNAME=root   (It’s username for the connection to the database)

MYSQL_DB_PASSWORD=root   (It’s password for the connection to the database)

MYSQL_DB_URL=jdbc:mysql://localhost/servletapi   (It’s connection url, you need to change here the name of the database (green color), and host (blue color) host ~99% will be localhost for your case, so, i think you will need to change the only database name)

MYSQL_DB_MAX_POOL_SIZE=15   (on the server created the connection pool to the database to optimize resource handling, so this property set the max value of opened connections)

MYSQL_DB_MIN_POOL_SIZE=2   (pool creates with 2 opened connections when the server will need more connections, it will open it automatically, in case if it won’t need for example 10 connections, it will close redundant. Connections are reusable because the creation of new one takes a lot of resources)

###Building and Deployment 

You need to install Java 8 and Maven onto your server.
Conﬁgure db.properties ﬁle You need to have tomcat server installed onto your system. 
Then you need to go to webapps folder in tomcat directory and move war ﬁle to this folder. (If tomcat has some folders in webapps directory, 
you can delete them in case if it isn’t your other war ﬁles))


###Database 
 
table name url  ﬁelds:  - id (pk)
 - url (varchar)
 - data (mediumtext)

###Save url endpoint
POST
endpoint: /url/save
body: {"url": "http://google.com"}
responses:   - success response: {"status": true}
- failure response: {"status": false, "data": "here is message about trouble"}
Short description:

When data comes to the endpoint, it is validating url in the body, if it won’t pass validation, the response will be with failure message and with status false.  When validation step passed, server downloading html page by url and saves it to the database.

###Get url endpoint  POST
endpoint: /url/get
body: {"url": “http://google.com"} (or it can be empty, when body is empty, server return all records from database)

responses:
- success response: {"status": true, data: [{“url”:”http://google.com"},{"url":"http:// google1.com"}]}
- failure response: {"status": false, "data": "here is message about trouble"}